// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "GameFramework/KillZVolume.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 70.f;
	MovementSpeed = 0.5;
	MoveDirection = EMovementDirection::DOWN;
	LastMoveDirection = EMovementDirection::DOWN;
	MovementVector = FVector::ZeroVector;
	bBonusSpeed = false;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(3);
	SetSpeed();
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int k = 0; k < ElementsNum; ++k)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		bool ZeroIndex = (SnakeElements.Add(NewSnakeElem)) == 0;
		 if (ZeroIndex)
		 {
		 	NewSnakeElem->SetFirstElementType();
		 }
	}
}

void ASnakeBase::Move()
{
	MovementVector = FVector::ZeroVector;
	if(MoveDirection == EMovementDirection::UP)
	{
		MovementVector.X += ElementSize;
	}
	else if(MoveDirection == EMovementDirection::DOWN)
	{
		MovementVector.X -= ElementSize;
	}
	else if(MoveDirection == EMovementDirection::LEFT)
	{
		MovementVector.Y += ElementSize;
	}
	else if(MoveDirection == EMovementDirection::RIGHT)
	{
		MovementVector.Y -= ElementSize;
	}

	FVector NextElementLocation = SnakeElements[0]->GetActorLocation();
	SnakeElements[0]->AddActorWorldOffset(MovementVector);

	for (int k = 1; k < SnakeElements.Num(); k++)
	{
		FVector ElementLocation = SnakeElements[k]->GetActorLocation();
		SnakeElements[k]->SetActorLocation(NextElementLocation);
		NextElementLocation = ElementLocation;
	}
	LastMoveDirection = MoveDirection;
}

void ASnakeBase::SetBonusSpeed()
{
	bBonusSpeed = true;
	SetSpeed();
	GetWorld()->GetTimerManager().SetTimer(BonusSpeedTimer, this, &ASnakeBase::CancelBonusSpeed, 5.0f, false);
}

void ASnakeBase::CancelBonusSpeed()
{
	bBonusSpeed = false;
	SetSpeed();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if(IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		AKillZVolume* KillZone = Cast<AKillZVolume>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
		else if(KillZone)
		{
			OverlappedElement->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::SetSpeed()
{
	if (bBonusSpeed)
	{
		GetWorld()->GetTimerManager().SetTimer(MovementTimer, this, &ASnakeBase::Move, MovementSpeed*0.7, true);
	}
	else
	{
		GetWorld()->GetTimerManager().SetTimer(MovementTimer, this, &ASnakeBase::Move, MovementSpeed, true);
	}
}


