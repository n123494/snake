// Fill out your copyright notice in the Description page of Project Settings.


#include "ClearBonus.h"
#include "SnakeBase.h"

// Sets default values
AClearBonus::AClearBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AClearBonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AClearBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AClearBonus::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if(IsValid(Snake) && bIsHead)
	{
		int32 ElementsCount = Snake->Pawn->StaticElements.Num();
		for(int k = 0; k < ElementsCount; k++)
		{
			Snake->Pawn->StaticElements[k]->Destroy();
		}
		Snake->Pawn->StaticElements.Empty();
	}
}

