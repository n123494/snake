// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
		
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementBase::SetFirstElementType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}

void ASnakeElementBase::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		APlayerPawnBase* Pawn = Snake->Pawn;
		if (IsValid(Snake))
		{
			TArray<AActor*> Elements;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), StaticClass(), Elements);
			int32 AllSnakeElements = Elements.Num();
			for (int k = 0; k < AllSnakeElements; k++)
			{
				Elements[k]->Destroy();
			}
			Pawn->StaticElements.Empty();
			Snake->Destroy();
			int32 HP = Pawn->SnakeHealth -= 1;
			FString Text = FString::Printf(TEXT("You have %s lives"), *FString::FromInt(HP));
			GEngine->AddOnScreenDebugMessage(0, 10.0f, FColor::Red, Text);
			if (Pawn->SnakeHealth != 0)
			{
				Pawn->CreateSnakeActor();
			}
		}
	}
}

void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
										AActor* OtherActor,
										UPrimitiveComponent* OtherComp,
										int32 OtherBodyIndex,
										bool bFromSweep,
										const FHitResult &SweepResult)
{
	if(IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
	}
}