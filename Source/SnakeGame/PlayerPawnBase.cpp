// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
	SnakeHealth = 5;
	Text = FString::Printf(TEXT("Press Q to drop tail"));
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
	SpawnFood();
	GEngine->AddOnScreenDebugMessage(0, 10.0f, FColor::Red, Text);
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	PlayerInputComponent->BindAction("FlipOffTail", IE_Pressed, this, &APlayerPawnBase::TailDrop);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
	SnakeActor->Pawn = this;
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if(IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection!=EMovementDirection::DOWN)
		{
			SnakeActor->MoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->MoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->MoveDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->MoveDirection = EMovementDirection::LEFT;
		}
	}
}

void APlayerPawnBase::TailDrop()
{
	int32 TailEnd = SnakeActor->SnakeElements.Num();
	for(int k = 3; k < TailEnd; k++)
	{
		ASnakeElementBase* StaticElement = SnakeActor->SnakeElements[k];
		int32 DestroyChance = FMath::RandRange(0, 100);
		if(DestroyChance < 50)
		{
			SnakeActor->SnakeElements[k]->Destroy();
		}
		else
		{
			StaticElements.Add(StaticElement);
		}
	}
	SnakeActor->SnakeElements.SetNum(3);
}


void APlayerPawnBase::SpawnFood()
{
	FVector FoodLocation (0, 0, 1);
	FoodLocation.X = FMath::RandRange(-5, 5) * SnakeActor->ElementSize;
	FoodLocation.Y = FMath::RandRange(-5, 5) * SnakeActor->ElementSize;
	FTransform FoodTransform (FoodLocation);
	int32 BonusType = FMath::RandRange(0, 100);
	if (BonusType < 20)
	{
		GetWorld()->SpawnActor<AFood>(SpeedBonusClass, FoodTransform);
	}
	else if (BonusType > 80)
	{
		GetWorld()->SpawnActor<AFood>(ClearBonusClass, FoodTransform);
	}
	else if ((BonusType >= 20) && (BonusType <= 80))
	{
		GetWorld()->SpawnActor<AFood>(ElementBonusClass, FoodTransform);
	}
}
