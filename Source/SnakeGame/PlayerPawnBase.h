// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"



#include "ClearBonus.h"
#include "ElementBonus.h"
#include "SnakeElementBase.h"
#include "SpeedBonus.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 SnakeHealth;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASpeedBonus> SpeedBonusClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AClearBonus> ClearBonusClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AElementBonus> ElementBonusClass;
	
	UPROPERTY()
	TArray<ASnakeElementBase*> StaticElements;

	UPROPERTY()
	FString Text;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);

	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	UFUNCTION()
	void TailDrop();
	
	void SpawnFood();
};
